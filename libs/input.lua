
input = {}
input.activedProfils = {}

local profil = {}
local profil_mt = {
	__index = profil,
	__type = "input-profil"
}

function input.newProfil(keys)
	local prof = setmetatable({}, profil_mt)

	prof.keys = {}
	prof.keysDown = {}

	for k, f in pairs(keys or {}) do
		prof:addKey(k, f.down, f.up)
	end

	return prof
end

function input.enableProfil(profil)
	for i, prof in pairs(input.activedProfils) do
		if profil == prof then
			table.remove(input.activedProfils, i)
			break
		end
	end

	table.insert(input.activedProfils, 1, profil)
end

function input.disableProfil(profil)
	for i, prof in pairs(input.activedProfils) do
		if profil == prof then
			table.remove(input.activedProfils, i)
			break
		end
	end
end

function input.keypressed(key, scancode, isrepeat)
	if input.superProfil then
		local override = input.superProfil:checkDown(key, scancode, isrepeat)
		if override then return end
	end

	for i, prof in pairs(input.activedProfils) do
		if prof ~= input.superProfil then
			local override = prof:checkDown(key, scancode, isrepeat)
			if override then return end
		end
	end
end

function input.keyreleased(key, scancode)
	if input.superProfil then
		local override = input.superProfil:checkUp(key, scancode)
		if override then return end
	end

	for i, prof in pairs(input.activedProfils) do
		if prof ~= input.superProfil then
			local override = prof:checkUp(key, scancode)
			if override then return end
		end
	end
end

function input.textinput(text)
	if input.superProfil then
		local override = input.superProfil:checkText(text)
		if override then return end
	end

	for i, prof in pairs(input.activedProfils) do
		if prof ~= input.superProfil then
			local override = prof:checkText(text)
			if override then return end
		end
	end
end

-- Input Profil

function profil:super()
	input.superProfil = self
end

function profil:enable()
	input.enableProfil(self)
end

function profil:disable()
	input.disableProfil(self)
end

function profil:addKey(key, down, up)
	self.keys[key] = {down = down, up = up}
end

function profil:checkText(text)
	if isfunction(self.textinput) then
		local override = self.textinput(text)
		if override then return true end
	end
end

function profil:checkDown(key, scancode, isrepeat)
	self.keysDown[key] = true

	local override
	if isfunction(self.keypressed) then
		override = self.keypressed(key, scancode, isrepeat)
		if override == true then return true end
	end

	if isnil(override) and self.keys[key] and isfunction(self.keys[key].down) then
		local override = self.keys[key].down(isrepeat)
		if override then return true end
	end
end

function profil:checkUp(key, scancode)
	self.keysDown[key] = nil

	local override
	if isfunction(self.keyreleased) then
		override = self.keyreleased(key, scancode)
		if override == true then return true end
	end

	if isnil(override) and self.keys[key] and isfunction(self.keys[key].up) then
		local override = self.keys[key].up()
		if override then return true end
	end
end

function profil:isKeyDown(key)
	return self.keysDown[key]
end

function profil:keysDown()
	local keys = {}
	for key, _ in pairs(self.keysDown) do
		table.insert(keys, key)
	end

	return keys
end
