game.config = {
	setting = {
		control = {
			menu = {
				up = {"control.menu.up", "up"},
				down = {"control.menu.down", "down"},
				right = {"control.menu.right", "right"},
				left = {"control.menu.left", "left"},
			}
		},

		video = {
			resolution = {"video.resolution", "1280x720"},
			screenmod = {"video.screenmod", "window"}
		}
	}
}
